import Foundation
import XCTest

@testable import Bowling

class ScoreCardTests: XCTestCase {
  
  var subject = ScoreCard()
  
  override func setUp() {
    subject = ScoreCard()
  }

  func testScoreCardFirstRollCorrectScore() throws {
    _ = try! subject.roll(.first(4))
    let score = try! subject.roll(.second(5))
    
    XCTAssertEqual(score, [9])
  }
  
  func testScoreCardFirstRollTwiceThrowsError() throws {
    _ = try! subject.roll(.first(5))
    
    do {
      try subject.roll(.first(5))
    } catch ScoreCard.Error.firstRollAgain {
      
    }
  }
  
  func testScoreCardSecondRollCorrectScore() throws {
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(9))
    let score = try! subject.roll(.second(1))
    
    XCTAssertEqual(score, [9, 19])
  }
  
  func testScoreCardThirdRollCorrectScore() throws {
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(9))
    _ = try! subject.roll(.second(1))
    
    let score = try! subject.roll(.first(10))
    
    XCTAssertEqual(score, [9, 19, 29])
  }
  
  func testWholeScoreCardNoStrikesOrSpares() throws {
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    let score = try! subject.roll(.second(5))
    
    
    XCTAssertEqual(score, [9, 18, 27, 36, 45, 54, 63, 72, 81, 90])
  }
  
  func testWholeScoreCard1Spare() throws {
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(5))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    let score = try! subject.roll(.second(5))
    
    
    XCTAssertEqual(score, [9, 18, 27, 36, 46, 50, 59, 68, 77, 86])
  }
  
  func testWholeScoreCard1Strike() throws {
    _ = try! subject.roll(.first(10))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    let score = try! subject.roll(.second(5))
    
    
    XCTAssertEqual(score, [10, 19, 28, 37, 46, 55, 64, 73, 82, 91])
  }
  
  func testWholeScoreCardSpareAtEnd() throws {
    _ = try! subject.roll(.first(10))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(6))
    
    let score = try! subject.roll(.first(6))
    
    XCTAssertEqual(score, [10, 19, 28, 37, 46, 55, 64, 73, 82, 92])
  }
  
  func testWholeScoreCardStrikeAtEnd() throws {
    _ = try! subject.roll(.first(10))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(4))
    _ = try! subject.roll(.second(5))
    
    _ = try! subject.roll(.first(10))
    
    _ = try! subject.roll(.first(6))
    let score = try! subject.roll(.second(6))
    
    XCTAssertEqual(score, [10, 19, 28, 37, 46, 55, 64, 73, 82, 92])
  }
  
  func testWholeScoreScoresFromSpec() throws {
    _ = try! subject.roll(.first(9))
    _ = try! subject.roll(.second(1))
    
    _ = try! subject.roll(.first(0))
    _ = try! subject.roll(.second(10))
    
    _ = try! subject.roll(.first(10))
    
    _ = try! subject.roll(.first(10))
    
    _ = try! subject.roll(.first(6))
    _ = try! subject.roll(.second(2))
    
    _ = try! subject.roll(.first(7))
    _ = try! subject.roll(.second(3))
    
    _ = try! subject.roll(.first(8))
    _ = try! subject.roll(.second(2))
    
    _ = try! subject.roll(.first(10))
    
    _ = try! subject.roll(.first(9))
    _ = try! subject.roll(.second(0))
    
    _ = try! subject.roll(.first(10))
    
    _ = try! subject.roll(.first(10))
    let score = try! subject.roll(.second(8))
    
    XCTAssertEqual(score, [10, 30, 56, 74, 82, 100, 120, 139, 148, 176])
  }
  
}
