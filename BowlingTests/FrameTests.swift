import XCTest

@testable import Bowling

class FrameTests: XCTestCase {

  func testIsSpareReturnsFalse() {
    let subject = Frame(first: 5, number: 1, previous: nil)
    subject.second = 4
    XCTAssertFalse(subject.isSpare)
  }
  
  func testFrameIsSpareReturnsTrue() {
    let subject = Frame(first: 5, number: 1, previous: nil)
    subject.second = 5
    XCTAssertTrue(subject.isSpare)
  }
  
  func testFrameIsStrikeReturnsFalse() {
    let subject = Frame(first: 9, number: 1, previous: nil)
    subject.second = 5
    XCTAssertFalse(subject.isStrike)
  }
  
  func testFrameIsStrikeReturnsTrue() {
    let subject = Frame(first: 10, number: 1, previous: nil)
    XCTAssertTrue(subject.isStrike)
  }
  
  func testFrameIsCompleteReturnsTrue() {
    let subject = Frame(first: 10, number: 1, previous: nil)
    subject.second = 3
    XCTAssertTrue(subject.isComplete)
  }
  
  func testFrameIsStrikeCompleteReturnsTrue() {
    let subject = Frame(first: 10, number: 1, previous: nil)
    XCTAssertTrue(subject.isComplete)
  }
  
  func testFrameIsSpareCompleteReturnsTrue() {
    let subject = Frame(first: 9, number: 1, previous: nil)
    subject.second = 1
    XCTAssertTrue(subject.isComplete)
  }
  
  func testFrameCompleteReturnsFalse() {
    let subject = Frame(first: 9, number: 1, previous: nil)
    XCTAssertFalse(subject.isComplete)
  }
  
  func testPreviousFrameIsStrikeCorrectScore() {
    let firstFrame = Frame(first: 10,
                           number: 1,
                           previous: nil)
    let subject = Frame(first: 4,
                        number: 2,
                        previous: firstFrame)
    subject.second = 5
    XCTAssertEqual(subject.score, 19)
  }
  
  func testPreviousFrameIsSpareCorrectScore() {
    let firstFrame = Frame(first: 9,
                           number: 1,
                           previous: nil)
    firstFrame.second = 1
    let subject = Frame(first: 4,
                        number: 2,
                        previous: firstFrame)
    subject.second = 5
    XCTAssertEqual(subject.score, 14)
  }
  
  func testPreviousNotStrikeOrSpareFrameCorrectScore() {
    let firstFrame = Frame(first: 5,
                           number: 1,
                           previous: nil)
    firstFrame.second = 1
    let subject = Frame(first: 4,
                        number: 2,
                        previous: firstFrame)
    subject.second = 5
    XCTAssertEqual(subject.score, 15)
  }
  
  func testSpareSpareStrikeNonStrikeOrSpare() {
    let firstFrame = Frame(first: 9,
                           number: 1,
                           previous: nil)
    firstFrame.second = 1
    XCTAssertEqual(firstFrame.score, 10)
    
    let secondFrame = Frame(first: 0,
                            number: 2,
                            previous: firstFrame)
    secondFrame.second = 10
    XCTAssertEqual(secondFrame.score, 20)
    
    let thirdFrame = Frame(first: 10,
                           number: 3,
                           previous: secondFrame)
    
    let subject = Frame(first: 6,
                        number: 4,
                        previous: thirdFrame)
    subject.second = 2
    XCTAssertEqual(subject.score, 82)
  }
    
}
