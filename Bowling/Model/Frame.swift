import Foundation

class Frame {
  
  let first: Int
  let number: Int
  var second: Int? = nil
  private var previous: Frame?
  
  //TODO: Stop unwrapping second all the time?
  var score: Int? {
    guard isComplete, number < 11 else { return nil }
    
    if let previous = previous,
      let previousScore = previous.score {
      if previous.isStrike {
        return previousScore + 10 + (first + (second ?? 0))
      }
      if previous.isSpare {
        return previousScore + 10 + first
      }
      return previousScore + (first + (second ?? 0))
    }
    return first + (second ?? 0)
  }
  
  var isComplete: Bool {
    return isStrike || isSpare || second != nil
  }
  
  var isSpare: Bool {
    return first + (second ?? 0) == 10
  }
  
  var isStrike: Bool {
    return first == 10
  }
  
  init(first: Int, number: Int, previous: Frame?) {
    self.first = first
    self.number = number
    self.previous = previous
  }
  
}
