import Foundation

enum Roll {
  case first(Int)
  case second(Int)
}

class ScoreCard {
  
  enum Error: Swift.Error {
    case firstRollAgain
  }
  
  var frame: Int {
    return currentFrame?.number ?? 0
  }
  
  var score: Int {
    return framesScores.reduce(0) {
      (acc: Int, score: Int) -> Int in
      return acc + score
    }
  }
  
  private var framesScores: [Int] {
    //refactor this
    return frames.reversed().compactMap { $0.score }.reversed()
  }
  var isGameOver = false
  
  private var lastRoll: Roll?
  
  private var frames: [Frame] = []
  private var currentFrame: Frame? {
    return frames.last
  }
  
  func roll(_ roll: Roll) throws -> [Int] {
    //TODO: throw error if first roll twice
    switch roll {
    case .first(let score):
      let frame = Frame(first: score,
                        number: frames.count + 1,
                        previous: currentFrame)
      frames.append(frame)
    case .second(let score):
      currentFrame?.second = score
    }
    return framesScores
  }
  
}

